# Introduction

This is an implementation to solve a maze by finding the shortest path using
the A* Heuristic Algorithm.

# Classes

    main/
        Maze -> This class is used to read and solve the maze
        Node -> The Node class is used to create the node object
                which represents a cell in the maze.
    test/
        MazeTest -> A number of Junit 5 tests to ensure that
                    all the requirements are satisfied and 
                    no error occurs in the program.

# Run instructions

Make sure you have installed Java 8 and the jdk is in the PATH.

#### On Windows cmd:

```
cd Maze_Solving\src
javac main\Maze.java
java main.Maze <path_to_mazes>
```

#### Example:

```
cd Maze_Solving\src
javac main\Maze.java
java main.Maze C:\test_files\medium_input.txt
```




