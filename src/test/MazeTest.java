package test;

import main.Maze;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.file.Paths;
import java.time.Duration;


import static org.junit.jupiter.api.Assertions.*;

class MazeTest {


    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @BeforeEach
    void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    void cleanUpStreams() {
        System.setOut(null);
    }

    @Test
    void testSmallInputMazeSolving(){
        assertTimeout(Duration.ofSeconds(30), () -> {
            String path = Paths.get("test_files","small.txt").toString();
            Maze small_maze = new Maze(path);
            small_maze.readFile();
            assertNotNull(small_maze.findPath());
        });
    }

    @Test
    void testMediumInputMazeSolving(){
        assertTimeout(Duration.ofSeconds(30), () -> {
            String path = Paths.get("test_files","medium_input.txt").toString();
            Maze medium_maze = new Maze(path);
            medium_maze.readFile();
            assertNotNull(medium_maze.findPath());
        });
    }

    @Test
    void testSparseMediumInputMazeSolving(){
        assertTimeout(Duration.ofSeconds(30), () -> {
            String path = Paths.get("test_files","sparse_medium.txt").toString();
            Maze sparse_maze = new Maze(path);
            sparse_maze.readFile();
            assertNotNull(sparse_maze.findPath());
        });
    }

    @Test
    void testLargeInputMazeSolving(){
        assertTimeout(Duration.ofSeconds(30), () -> {
            String path = Paths.get("test_files","large_input.txt").toString();
            Maze large_maze = new Maze(path);
            large_maze.readFile();
            assertNotNull(large_maze.findPath());
        });
    }

    @Test
    void testNormalInputSolving(){
        assertTimeout(Duration.ofSeconds(30), () -> {
            String path = Paths.get("test_files","input.txt").toString();
            Maze maze = new Maze(path);
            maze.readFile();
            assertNotNull(maze.findPath());
        });
    }

    @Test
    void testOutputShortestPath(){
        String sampleOutput =   "##########\r\n" +
                                "#SXX     #\r\n" +
                                "# #X######\r\n" +
                                "# #XX    #\r\n" +
                                "# ##X# ###\r\n" +
                                "# # X# # #\r\n" +
                                "# # XX   #\r\n" +
                                "# ###X####\r\n" +
                                "# #  XXXE#\r\n" +
                                "##########\r\n";
        String path = Paths.get("test_files","sample.txt").toString();
        Maze maze = new Maze(path);
        maze.readFile();
        maze.printMaze(maze.findPath());
        assertEquals(sampleOutput, outContent.toString());
    }

    @Test
    void testMazeWithNoSolution(){
        String sampleOutput = "End position cannot be reached. This maze has no solution.";
        String path = Paths.get("test_files","no_path_input.txt").toString();
        Maze maze = new Maze(path);
        maze.readFile();
        maze.printMaze(maze.findPath());
        assertEquals(sampleOutput, outContent.toString().replace("\r\n",""));
    }
}