/**
 * @author: Dimitrios Kontogiannidis
 * @version: 29/10/2017
 **/
package main;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * A class used to read mazes from a plaintext file, use the A* Heuristic Algorithm to find the shortest path between
 * a starting and an ending position, and print the resulting path to an output file.
 **/
public class Maze {

    private final String path;      // path of the file
    private int width;              // number of nodes horizontally
    private int height;             // number of nodes vertically
    private int[] start;            // [y, x] coordinates of starting point
    private int[] end;              // [y, x] coordinates of ending point
    private String[][] mazeMatrix;  // The matrix representation of the maze.

    /**
     * Class constructor
     **/
    public Maze(String path){
        this.path = path;
    }

    /**
     * Read a file containing the maze in a format with 0 and 1 values, where 1 is a wall and 0 is a traversable path.
     * Upon reading the maze from the file save it in a 2D string array format.
     **/
    public void readFile(){

        try {
            FileReader maze_file = new FileReader(path);
            BufferedReader maze_buffer = new BufferedReader(maze_file);

            String[] shape = maze_buffer.readLine().split(" ");

            width = Integer.parseInt(shape[0]);
            height = Integer.parseInt(shape[1]);

            start = stringArrayToIntArray(maze_buffer.readLine().split(" "));
            end = stringArrayToIntArray(maze_buffer.readLine().split(" "));

            if (start.length != 2 || end.length != 2 || shape.length != 2) {
                throw new IOException();
            }
            // Create the maze matrix.
            String line;
            mazeMatrix = new String[height][width];
            for (int i=0; (line=maze_buffer.readLine()) != null; i++){
                mazeMatrix[i] = line.split(" ");
            }
            maze_buffer.close();
            maze_file.close();
        }
        catch (FileNotFoundException ex){
            System.out.println("File does not exist.");
            System.exit(-1);
        }
        catch (IOException ex){
            System.out.println("File could not be read.");
            System.exit(-1);
        }
    }

    /**
     * This method searches for the shortest path from a start point to an end point using the A* Algorithm.
     * If a path exists then it returns a single node which references its parent node else it returns null
     * indicating that a path between the start node and the end node does not exist. The path between those
     * two points can then be retrieved by following the reference chain that occurs from the parent nodes.
     *
     * @return : A single Node object or null.
     **/
    public Node findPath(){
        // The queue containing the nodes to be visited prioritized upon their f score.
        PriorityQueue<Node> openQueue = new PriorityQueue<>(Comparator.comparing(Node::getfScore));
        // The list containing the nodes that are already visited
        List<Node> closedList = new ArrayList<>();
        // Add the starting node to the queue.
        openQueue.add(new Node(start[1], start[0], null, 0.0, 0.0));
        // Create the target (end) node.
        Node targetNode = new Node(end[1], end[0], null, 0.0, 0.0);
        // While the queue has more nodes to be evaluated.
        while (!openQueue.isEmpty()){
            // Pop the node with the minimum fScore in the openQueue.
            Node bestNode = openQueue.poll();
            // Add appropriate neighbors to the openQueue and check if they are the target node.
            for (Node neighbor : generateNeighbors(bestNode)){
                // If the neighbor is the end node then return it and finish the search.
                if (neighbor.getxCoordinate()==end[1] && neighbor.getyCoordinate()==end[0]){
                    return neighbor;
                }
                // Calculate the g, h and f scores for the neighboring node.
                //  The euclidean distance of the neighbor from the start node.
                neighbor.setgScore(neighbor.getParent().getgScore() + nodeDistance(neighbor.getParent(), neighbor));
                //  The euclidean distance of the neighbor from the end node.
                neighbor.sethScore(nodeDistance(targetNode, neighbor));
                //  The sum of the neighbor node distance from start node (g) and the distance from the end node (h).
                neighbor.calculatefScore();

                // If a node with the same position as successor is in the openQueue or the closedList which has a lower
                // f than successor, skip this successor as it is already visited and it's f score did not improve since
                // the last visit.
                int openSetOccurrences = openQueue.stream()
                        .filter(node -> ((nodeDistance(node, neighbor)==0) && (node.getfScore() <= neighbor.getfScore())))
                        .collect(Collectors.toList()).size();
                int closedSetOccurrences = closedList.stream()
                        .filter(node -> ((nodeDistance(node, neighbor)==0) && (node.getfScore() <= neighbor.getfScore())))
                        .collect(Collectors.toList()).size();
                if (!(openSetOccurrences + closedSetOccurrences > 0)){
                    openQueue.add(neighbor);    // If an appropriate node is found add it to the openQueue.
                }
            }
            // When all the neighbors of the bestNode are evaluated add the bestNode to the closedList, meaning it is
            // a visited node.
            closedList.add(bestNode);
        }
        return null;
    }

    /**
     * Check if neighbors exist in the North, South, West, East directions and add them to the neighbors
     * list. Each neighbor will then be evaluated individually in the findPath() method.
     *
     * @param bestNode :    The node object to be used for searching its neighbors.
     **/
    private List<Node> generateNeighbors(Node bestNode){
        // List of the neighboring nodes to the bestNodes
        List<Node> neighbors = new ArrayList<>();
        // The coordinates in the x, y axis of the bestNode
        int xCoordinate = bestNode.getxCoordinate();
        int yCoordinate = bestNode.getyCoordinate();
        // North Neighbor
        double inf = Double.POSITIVE_INFINITY;
        if (yCoordinate + 1 < width && mazeMatrix[xCoordinate][yCoordinate+1].equals("0")) {
            neighbors.add(new Node(xCoordinate, yCoordinate + 1, bestNode, inf, inf));
        }
        // South Neighbor
        if (yCoordinate - 1 >= 0 && mazeMatrix[xCoordinate][yCoordinate-1].equals("0")){
            neighbors.add(new Node(xCoordinate, yCoordinate-1, bestNode, inf, inf));
        }
        // West Neighbor
        if (xCoordinate - 1 >= 0 && mazeMatrix[xCoordinate-1][yCoordinate].equals("0")){
            neighbors.add(new Node(xCoordinate-1, yCoordinate, bestNode, inf, inf));
        }
        // East Neighbor
        if (xCoordinate + 1 < height  && mazeMatrix[xCoordinate+1][yCoordinate].equals("0")){
            neighbors.add(new Node(xCoordinate+1, yCoordinate, bestNode, inf, inf));
        }
        return neighbors;
    }

    /**
     * Calculates the Euclidean distance between 2 nodes in the maze. Mainly used as a helper method for
     * the calculation of the g, h and f scores in the A* algorithm.
     *
     * @param firstNode     : The first node object for the comparison.
     * @param secondNode    : The second node object for the comparison.
     * @return              : The euclidean distance as a double value.
     **/
    private double nodeDistance(Node firstNode, Node secondNode){
        double xAxisDistance = Math.pow(firstNode.getxCoordinate()-secondNode.getxCoordinate(), 2);
        double yAxisDistance = Math.pow(firstNode.getyCoordinate()-secondNode.getyCoordinate(), 2);
        return Math.sqrt(xAxisDistance+yAxisDistance);
    }


    /**
     * Helper function to transform an array of Strings to an array of integers by mapping the
     * Integer.parseInt function.
     *
     * @param stringArray   : An array of strings (String[]).
     * @return              : An array of integers (int[]).
     **/
    private static int[] stringArrayToIntArray(String[] stringArray){
        return Arrays.stream(stringArray).mapToInt(Integer::parseInt).toArray();
    }

    /**
     * Prints the maze in the following format:
     *
     *  #####
     *  #SXX#
     *  # #X#
     *  # #X#
     *  # #E#
     *  #####
     *
     *  "#" is a wall
     *  " " is a traversable path
     *  "S" is the starting point
     *  "E" is the ending point
     *  "X" is a positioned traversed to go from S to E.
     *
     * @param node  : A Node object provided as a solution to the maze (usually from the findPath()).
     **/
    public void printMaze(Node node){
         String[][] maze = mazeMatrix;
         if (node!=null) {
             while ((node = node.getParent()) != null) {
                 maze[node.getxCoordinate()][node.getyCoordinate()] = "X";
             }
             maze[start[0]][start[1]] = "S";
             maze[end[1]][end[0]] = "E";
             for (String[] row: maze){
                 for (String cell: row){
                     switch (cell) {
                         case "0":
                             System.out.print(" ");
                             break;
                         case "1":
                             System.out.print("#");
                             break;
                         default:
                             System.out.print(cell);
                             break;
                     }
                 }
                 System.out.println();
             }
         }
         else {
             System.out.println("End position cannot be reached. This maze has no solution.");
         }
    }

    public static void main(String args[]){
        if (args.length == 1){
            Maze mp = new Maze(args[0]);
            mp.readFile();
            mp.printMaze(mp.findPath());
        }
        else {
            System.out.println("ERROR: Only one argument must be provided including the path of the maze file.");
            System.exit(-1);
        }

    }
}
