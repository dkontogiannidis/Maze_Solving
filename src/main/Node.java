/**
 * @author: Dimitrios Kontogiannidis
 * @version: 29/10/2017
 **/

package main;


public class Node{

    private final int xCoordinate, yCoordinate; // X,Y coordinates of the node.
    private final Node parent;  // The node that was visited before this node.
    private double fScore;      // The sum of distance from start node and distance from end node (g+h).
    private double hScore;      // The distance from the end node.
    private double gScore;      // The distance from the start node.

    public Node(int xCoordinate, int yCoordinate, Node parent, double gScore, double hScore){
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.parent = parent;
        this.gScore = gScore;
        this.hScore = hScore;
        fScore = gScore + hScore;
    }

    public double getfScore() {
        return fScore;
    }

    public Node getParent() {
        return parent;
    }

    public int getxCoordinate() {
        return xCoordinate;
    }

    public int getyCoordinate() {
        return yCoordinate;
    }

    public void setgScore(double gScore) {
        this.gScore = gScore;
    }

    public void sethScore(double hScore) {
        this.hScore = hScore;
    }

    public void calculatefScore() {
        this.fScore = gScore + hScore;
    }

    public double getgScore() {
        return gScore;
    }
}